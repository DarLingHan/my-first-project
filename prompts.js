module.exports = [
    {
        type: 'list',
        name: 'module',
        message: '请选择你需要生成的模版',
        choices: [
            {name: '模版-黄色', value: 'module-yellow'},
            {name: '模版-绿色', value: 'module-green'},
        ],
        default: 'module-yellow'
    },
    {
        type: 'input',
        name: 'projectId',
        message: '请输入项目Id',
        default: '14'
    }
]