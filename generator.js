// api: 一个generatorAPI实列
// options: prompts问题数组的用户输入组合成的选项对象
// rootOptions: 整个preset.json对象

module.exports = (api, options, rootOptions) => {
    api.extendPackage({
        scripts: {
            build: 'vue-cli-service build'
        }
    })
    
    // 根据命令行输入渲染对应的模版、设置全局变量
    let configParams = {
        PROJECT_ID: options.projectId
    }
    api.render(`./template/${options.module}`, configParams)
}